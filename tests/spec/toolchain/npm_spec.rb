# frozen_string_literal: true

require 'spec_helper'

context 'npm packages' do
  describe package('yarn') do
    it { should be_installed.by('npm') }

    it 'should install a package.json' do
      with_fixtures do |fixture_dir|
        cmd = command("cd #{fixture_dir}/yarn && yarn install")
        expect(cmd).to be_a_successful_cmd
        expect(cmd.stdout).to match(/Done in/)

        file = file("#{fixture_dir}/yarn/yarn.lock")
        expect(file).to be_file
      end
    end
  end
end
