# frozen_string_literal: true

require 'spec_helper'

context 'spotlight' do
  # check that spotlight indexing is disabled
  describe command('sudo mdutil -s /') do
    it { should be_a_successful_cmd }
    its(:stdout) { should match(/Indexing disabled/) }
  end
end
