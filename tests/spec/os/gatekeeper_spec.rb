# frozen_string_literal: true

require 'spec_helper'

context 'gatekeeper' do
  # check that gatekeeper is disabled
  describe command('spctl --status') do
    its(:stdout) { should match(/assessments disabled/) }
  end
end
