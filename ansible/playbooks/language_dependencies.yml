---

- name: Get ruby prefix
  shell: . {{ ansible_user_dir }}/.asdf/asdf.sh && asdf where ruby
  register: ruby_prefix
  changed_when: false

- name: Get gem path
  command: "{{ ruby_prefix.stdout }}/bin/gem env gemdir"
  register: gem_path
  changed_when: false

- name: Load ruby in .zshrc
  blockinfile:
    dest: "{{ ansible_user_dir }}/.zshrc"
    marker: "# {mark} ANSIBLE MANAGED BLOCK: ruby"
    block: |
      export PATH="{{ ruby_prefix.stdout }}/bin:$PATH"
      export PATH="{{ gem_path.stdout }}/bin:$PATH"

- name: Load ruby in .bash_profile
  blockinfile:
    dest: "{{ ansible_user_dir }}/.bash_profile"
    marker: "# {mark} ANSIBLE MANAGED BLOCK: ruby"
    block: |
      export PATH="{{ ruby_prefix.stdout }}/bin:$PATH"
      export PATH="{{ gem_path.stdout }}/bin:$PATH"

- name: Install ruby deps
  gem:
    name: "{{ item.name | default(item) }}"
    version: "{{ item.version | default(omit) }}"
    executable: "{{ ruby_prefix.stdout }}/bin/gem"
    user_install: false
  loop: "{{ ruby_gems }}"

- name: Get python prefix
  command: "{{ homebrew_prefix }}/bin/brew --prefix python"
  register: python_prefix
  changed_when: false

- name: Load python in .zshrc
  blockinfile:
    dest: "{{ ansible_user_dir }}/.zshrc"
    marker: "# {mark} ANSIBLE MANAGED BLOCK: python"
    block: |
      export PATH="{{ python_prefix.stdout }}/bin:$PATH"
      export PATH="{{ python_prefix.stdout }}/libexec/bin:$PATH"

- name: Load python in .bash_profile
  blockinfile:
    dest: "{{ ansible_user_dir }}/.bash_profile"
    marker: "# {mark} ANSIBLE MANAGED BLOCK: python"
    block: |
      export PATH="{{ python_prefix.stdout }}/bin:$PATH"
      export PATH="{{ python_prefix.stdout }}/libexec/bin:$PATH"

- name: Install python deps
  pip:
    name: "{{ item.name | default(item) }}"
    version: "{{ item.version | default(omit) }}"
    executable: "{{ python_prefix.stdout }}/bin/pip3"
  loop: "{{ pip_packages }}"

- name: Configure git-lfs
  command: "{{ homebrew_prefix }}/bin/git-lfs install"

- name: Get dotnet prefix
  command: "{{ homebrew_prefix }}/bin/brew --prefix dotnet"
  register: dotnet_prefix
  changed_when: false

- name: Set DOTNET_ROOT in .zshrc
  blockinfile:
    dest: "{{ ansible_user_dir }}/.zshrc"
    marker: "# {mark} ANSIBLE MANAGED BLOCK: dotnet"
    block: |
      export DOTNET_ROOT="{{ dotnet_prefix.stdout }}/libexec"

- name: Set DOTNET_ROOT in .bash_profile
  blockinfile:
    dest: "{{ ansible_user_dir }}/.bash_profile"
    marker: "# {mark} ANSIBLE MANAGED BLOCK: dotnet"
    block: |
      export DOTNET_ROOT="{{ dotnet_prefix.stdout }}/libexec"

- name: Install Flutter versions
  command: "{{ homebrew_prefix }}/bin/fvm install {{ item }}"
  loop: "{{ flutter.versions }}"

- name: Configure global Flutter version
  command: "{{ homebrew_prefix }}/bin/fvm global {{ flutter.global }}"
