job-images
==========

This project maintains our Tart images used with [nesting](https://gitlab.com/gitlab-org/fleeting/nesting) on our MacOS Runners in AWS (starting with macOS 13 Ventura). This project can also be used by self-managed customers to build their own macOS runner machines.

The code is organized as follows:

* `ansible/` Ansible configuration and playbooks
* `packer/` macOS version specific Packer configuration and build scripts
* `tests/` tests ensuring expected configurations
* `toolchain/` job image configuration definitations

## How it works

This project uses [Packer](https://www.packer.io/), [Ansible](https://www.ansible.com/), and [Tart](https://tart.run/) to create a macOS machine image from an [IPSW](https://en.wikipedia.org/wiki/IPSW) file directly from Apple. The generated machine image is optimized to be run as a GitLab SaaS Runner, as well as a virtual machine in Tart that can be run on self-managed hardward and connected to self-managed or SaaS GitLab projects.

## What's included?

The inventory for each machine is described in the yaml files in the `toolchain` directory. Currently only macOS 13 Ventura is included, but this will be expanded in the future.

* [toolchain/macos-13.yml](https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops/job-images-poc/-/blob/main/toolchain/macos-13.yml)

## How to build an image

### Prerequisites

To build and run these virtual machines, you'll need to have Tart and Packer installed on the host machine. They can both be installed via Homebrew:
 
```
brew install cirruslabs/cli/tart packer
```

You'll also need to install the required Packer plugins. This can be done by running:

```
packer init packer/templates/macos-13-base.pkr.hcl
```

### IPSW and XIP Files

Apple uses IPSW and XIP files to distribute base installations for macOS and Xcode. These files are quite large and can be time consuming to download. It's recommended that you download the needed source files to the host machine to speed up the process. This also bypasses the need to provide login credentials to the App Store in order to install Xcode. [Apple Wiki](https://theapplewiki.com/wiki/Firmware/Mac/) has links to all the macOS firmware versions, and use [xcodes](https://github.com/XcodesOrg/xcodes) to download Xcode XIP files (Apple ID login required).

The location of these files can be configured in the `packer/conf/{version}.pkrvars.hcl` files.

For example:

```
/Volumes/base_images/UniversalMac_12.6_21G115_Restore.ipsw
/Volumes/base_images/UniversalMac_13.5_22G74_Restore.ipsw
/Volumes/base_images/UniversalMac_14.0_23A5257q_Restore.ipsw
/Volumes/base_images/xcode/Xcode_13.4.1.xip
/Volumes/base_images/xcode/Xcode_14.3.1.xip
/Volumes/base_images/xcode/Xcode_15_beta.xip
```

## Build a Machine Image

1. Start by building a base image based on an IPSW supplied by Apple. This step automates the standard onboarding flow of a new macOS machine. This process takes some time, so we'll create a snapshot once done and run the subsequent steps from the snapshot.
    
    ```
    $ packer build -var-file="packer/conf/macos-13.pkrvars.hcl" packer/templates/macos-13-base.pkr.hcl
    ```

1. Next, run the `toolchain` template to install all development tools. This will include Homebrew, ruby, python, go, etc.

    ```
    $ packer build -var-file="packer/conf/macos-13.pkrvars.hcl" packer/templates/toolchain.pkr.hcl
    ```

1. Next, run the `xcode` template to install the version of Xcode for the image.

    ```
    $ packer build -var-file="packer/conf/xcode-14.pkrvars.hcl" -var-file="packer/conf/macos-13.pkrvars.hcl" packer/templates/xcode.pkr.hcl
    ```
    
    The result of this step will be a machine image you can run in Tart called `macos-13-xcode-14`.
    
1. Finally, register and start the GitLab Runner to start picking up jobs.

	1. Create a new runner registration in your projects CI/CD settings. This process will generate a runner registration token (`YOUR_TOKEN`) to be used later.
	1. Start the virtual machine

        ```
        tart run macos-13-xcode-14
        ```
    
    1. SSH to the virtual machine, and register the GitLab Runner. Note: the ssh password is `gitlab`.

        SSH to the instance: 
        
        ```
        ssh gitlab@$(tart ip macos-13-xcode-14) 
        ```

		  Register the GitLab Runner:

        ```
        gitlab-runner register -n --url https://gitlab.com --executor shell --shell bash --token YOUR_TOKEN
        ```
        
    1. Start up the gitlab-runner and the runner will start picking up jobs

        ```
        gitlab-runner run
        ```

## Testing

Tests are included to ensure the all provisioned software is installed and functioning correctly. Test can be run on the host machine against any built virtual machine. `sshpass` is required to run the tests and can be installed via Homebrew:

```sh
brew tap gitlab/shared-runners https://gitlab.com/gitlab-org/ci-cd/shared-runners/homebrew.git
brew install gitlab/shared-runners/sshpass
```

To run the tests, boot up the virtual machine with (`macos-13-toolchain` is the image in this example):

```sh
tart run macos-13-toolchain
```

Then run:

```sh
TARGET_HOST=$(tart ip macos-13-toolchain) TARGET_PORT=22 LOGIN_PASSWORD=gitlab bundle exec rspec spec
```